DROP TABLE pays;
CREATE TABLE pays (
	ident_pays smallint,
	libelle varchar(100)
);

INSERT INTO pays VALUES
	(1, 'FRANCE');
INSERT INTO pays VALUES
	(2, 'USA');
INSERT INTO pays VALUES
	(3, 'ALGERIE');