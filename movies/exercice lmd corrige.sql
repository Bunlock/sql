
-- 1. Sélectionner toutes les infos sur les films réalisés par un réalisateur français triés par le nom du film.
select * 
from film f, realisateur r, pays p 
where f.ident_realisateur = r.ident_realisateur and f.pays = p.ident_pays 
and r.nationalite = 1
order by f.titre asc; 

-- 2. Sélectionner le nom du film, la date de sortie, le nom du réalisateur, le nom des acteurs, leur date de naissance, ainsi que le budget du film. 
-- Le tout trié en ordre descendant par titre du film et nom des acteurs.
select f.titre, f.date_sortie, r.nom, a.nom, a.date_naissance, s.budget
from film f, realisateur r , acteur a, statistique s, casting c 
where f.ident_film = s.ident_film 
and r.ident_realisateur = f.ident_realisateur
and c.ident_film = f.ident_film
and a.ident_acteur = c.ident_acteur
order by f.titre desc, a.nom desc;

-- 3. Trouver le nombre d'acteurs par film dans la base de données. 
-- Afficher le titre, la date de sortie, le nom du réalisateur et le distributeur.
select f.titre, f.date_sortie, r.nom, f.distributeur 
from film f, realisateur r
where f.ident_realisateur = r.ident_realisateur;

-- 4. Sélectionner le titre du film,la date de sortie, le nom et prénom du réalisateur,
-- le nom et prénom de l'acteur, sa date de naissance, le budget du film 
-- et le nombre d'entrées en France des films qui ont un acteur algérien.
select f.titre, f.date_sortie, CONCAT(r.prenom, ' ', r.nom) as realisateur,
CONCAT(a.prenom,' ', a.nom), a.date_naissance, s.nb_entree_france, s.budget 
from film f, realisateur r, acteur a, casting c, statistique s 
where f.ident_realisateur = r.ident_realisateur
and a.ident_acteur = c.ident_acteur
and c.ident_film = f.ident_film 
and s.ident_film = f.ident_film
and a.nationalite = 3
;

-- 5. Sélectionner le film qui a réalisé la recette la plus élevée dans le monde.
select f.titre, s.recette_monde 
from film f, statistique s
where f.ident_film = s.ident_film
order by s.recette_monde desc
limit 1;

-- 6. Sélectionner l'acteur qui a joué dans deux films différents
select a.nom, count(a.ident_acteur) as nbFilms
from acteur a, film f, casting c
where a.ident_acteur = c.ident_acteur
and f.ident_film = c.ident_film
group by a.nom
having count(a.ident_acteur) = 2
;

-- 7. Sélectionner la personne qui est à la fois réalisateur et acteur
select r.nom, a.nom
from acteur a, realisateur r
where a.nom = r.nom;

-- 8. Sélectionner les acteurs qui ont joué dans des films dont le nom du film
--  commence par la lettre S. 
-- Indiquer leur rôle et leur nationalité.
select a.nom, c.`role`, p.libelle, f.titre 
from acteur a, film f, casting c, pays p
where a.ident_acteur = c.ident_acteur 
and f.ident_film = c.ident_film
and f.pays = p.ident_pays
and f.titre like 'S%';

-- 9. Sélectionner les acteurs qui sont nés entre 1948 et mai 1978 
-- ainsi que le nombre de jours total de tournage qu'ils ont réalisés.
select a.nom, a.date_naissance, sum(c.nb_jour_tournage) as nbTotalJrTournage
from acteur a , casting c 
where a.ident_acteur =c.ident_acteur 
and a.date_naissance between '1948-01-01' and '1978-05-31'
group by a.nom;

