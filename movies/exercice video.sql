CREATE table or REPLACE `films` (
  `identFilm` int(11) NOT NULL,
  `titre` varchar(100) DEFAULT NULL,
  `genre1` varchar(100) DEFAULT NULL,
  `recette` float DEFAULT 0,
  `dateSortie` date DEFAULT NULL,
  `pays` int(11) DEFAULT NULL,
  `nbEntrees` int(11) DEFAULT NULL,
  `dateHeureSaisie` datetime DEFAULT NULL,
  `resume` mediumtext DEFAULT NULL,
  PRIMARY KEY (`identFilm`),
  KEY `films_genre1_IDX` (`genre1`,`pays`) USING BTREE,
  KEY `fkRealisateur` (`numReal`)
);

create or replace index genre1 ON films(pays);

alter table films add column numReal int(11);

create table if not exists realisateur (
	idReal int primary key
)

alter table films add constraint fkRealisateur foreign key (numReal) references realisateur(idReal);

alter table films alter column recette set default 0;

alter table films drop constraint fkRealisateur;