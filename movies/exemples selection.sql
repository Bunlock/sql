-- JOINTURES
select * from casting c, acteur a
where a.ident_acteur = c.ident_acteur

-- equivalent:
select * from casting
inner join acteur on casting.ident_acteur = acteur.ident_acteur 

-- equivalent, les 2 colonnes id acteur ont le meme nom:
select * from casting c natural join acteur a

-- les acteurs par film:
select a.nom, a.prenom, f.titre from acteur a, film f, casting c
where a.ident_acteur = c.ident_acteur 
and f.ident_film  = c.ident_film

-- en utilisant un groupby:
select * from casting c, acteur a group by c.ident_acteur

-- en utilisant un distinct:
select distinct casting.ident_acteur from casting, acteur

-- le nombre d'acteurs par film:
select count(a.ident_acteur) as nbActeurs, f.titre 
from acteur a, film f, casting c
where a.ident_acteur = c.ident_acteur 
and f.ident_film  = c.ident_film
group by f.titre
order by nbActeurs desc
