-- population fr 2012 :
select sum(v.ville_population_2012) from villes v;


-- Obtenir la liste des 10 villes les plus peuplées en 2012
select ville_nom_simple, ville_population_2012
from villes v
group by ville_nom_simple
order by ville_population_2012 desc limit 10 
;

-- Obtenir la liste des 50 villes ayant la plus faible superficie
select v.ville_nom_simple, v.ville_surface
from villes v 
order by v.ville_surface 
asc limit 50;

-- Obtenir la liste des départements d’outres-mer, c’est-à-dire ceux dont le numéro de département commencent par “97”
select d.departement_nom, d.departement_code from departement d where d.departement_code like '97%';

-- Obtenir le nom des 10 villes les plus peuplées en 2012, ainsi que le nom du département associé
create view top10villes as select ville_nom_simple, ville_population_2012, d.departement_nom
from villes v, departement d
where d.departement_code = v.ville_departement 
group by ville_nom_simple
order by ville_population_2012 desc limit 10;

-- Obtenir la liste du nom de chaque département, associé à son code et du nombre de commune au sein de ces département, en triant afin d’obtenir en priorité les départements qui possèdent le plus de communes
select d.departement_nom, d.departement_code, count(1) nbCommunes
from departement d, villes v
where d.departement_code = v.ville_departement 
group by d.departement_nom
order by nbCommunes desc;

-- variante : compter les communes par dpt et au total:
select d.departement_nom, d.departement_code, count(1) nbCommunes
from departement d, villes v
where d.departement_code = v.ville_departement 
group by d.departement_nom with rollup;

-- Obtenir la liste des 10 plus grands départements, en terme de superficie
select d.departement_nom, sum(v.ville_surface) superficie 
from departement d, villes v
where d.departement_code = v.ville_departement 
group by d.departement_nom
order by superficie desc
-- limit 10
;
-- Compter le nombre de villes dont le nom commence par “Saint”
select count(ville_nom) from villes
where ville_nom like 'Saint%'
;

-- Obtenir la liste des villes qui ont un nom existants plusieurs fois, 
-- et trier afin d’obtenir en premier 
-- celles dont le nom est le plus souvent utilisé par plusieurs communes
select ville_nom, count(1) nbVilles from villes
group by ville_nom order by nbVilles desc
;

-- Obtenir en une seule requête SQL la liste des villes dont la superficie
-- est supérieur à la superficie moyenne
select ville_nom, ville_surface from villes
where ville_surface > (select avg(ville_surface) from villes);


-- Obtenir la liste des départements qui possèdent plus de 2 millions d’habitants
select d.departement_nom_uppercase, sum(v.ville_population_2012) dptPop
from departement d, villes v
where d.departement_code = v.ville_departement
group by departement_code
having dptPop > 2000000;

-- Remplacez les tirets par un espace vide, 
-- pour toutes les villes commençant par “SAINT-” 
-- (dans la colonne qui contient les noms en majuscule)
select ville_nom from villes v where ville_nom like 'SAINT %';

update villes set ville_nom = replace(ville_nom, 'SAINT-', 'SAINT ')
where ville_nom like 'SAINT-%';


