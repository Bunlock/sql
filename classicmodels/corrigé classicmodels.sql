-- Find all the French customers
select * from customers where country = 'France';

-- Find first name and last name on employees with the title ”sales rep”
select e.firstName, e.lastName, e.jobTitle  from employees e where jobTitle = "sales rep";

-- Find name and description of the motorcycle models that cost between 50$ and 100$
select productName, productDescription  from products where productLine = 'Motorcycles'
and MSRP between 50 and 100;

-- Find the customer numbers for the orders that have been cancelled
select customerNumber from orders o where o.status = 'cancelled';

-- Find product lines and vendor for products with size 1:18
select productLine, productVendor  from products p where productScale ='1:18';

-- List all the motorcycles sorted by product name
select *  from products where productLine = 'Motorcycles'
order by productName;

-- List models with less than 1000 in stock sorted by 
-- quantity in stock with highest quantity at the top of the list
select * from products where quantityInStock < 1000
order by quantityInStock DESC;

-- List the Norwegian customers’ customer name and contact person sorted by the contact person’s first name
select customerName,
concat(contactFirstName,' ',contactLastName) as contact
 from customers where country = 'Norway'
order by contactFirstName;

-- How many German customers are there?
select count(*) from customers c where c.country = 'Germany';

-- What is the average price for classic car models?
select avg(p.MSRP) from products p where productLine = 'Classic Cars';

-- What is the price of the most expensive model from ‘Autoart Studio Design’?
select * from products p where productVendor = 'Autoart Studio Design'
order by p.MSRP desc limit 1;

-- How many different countries do the customers come from?
select count(distinct country) from customers c;

-- What is the quantity in stock for 1:12 models?
select sum(quantityInStock) from products p where productScale ='1:18';

-- What is the highest profit amongst the products?
select MSRP - buyPrice as profit, productName from products p 
order by profit desc limit 1;

-- What is the average price for each product line?
select productLine, avg(p.MSRP)  from products p
group by productLine ;

-- How many different products does each vendor have?
select productVendor, count(productVendor) from products p
group by productVendor;

-- What is the profit percentage wise based on product scale?
select p.productScale, avg(100 - (p.buyPrice / p.MSRP) * 100) as 'profit %' from products p
group by productScale;

-- How many orders exist for each order status type?
select o.status, count(1) as "# orders" from orders o
group by o.status;

-- How many orders do each customer have?
select o.customerNumber, count(1) as '# orders' from orders o group by o.customerNumber

-- JOIN
-- Find all customer names and the name of the employee responsible for the customer
select c.customerName, e.lastName from customers c, employees e 
where c.salesRepEmployeeNumber = e.employeeNumber; 

-- Find all customer names and the name of the employee responsible for the customer for italian customers
select c.customerName, e.lastName from customers c, employees e 
where c.salesRepEmployeeNumber = e.employeeNumber
and c.country = 'Italy'; 

-- Find all customer countries and the names of the employees responsible for customers in the countries
select c.country, e.lastName from customers c, employees e
where c.salesRepEmployeeNumber = e.employeeNumber 
group by c.country;

-- UPDATE 
-- Update Leslie Jennings’s last name to “Smith”
update employees set lastName ="Smith" where lastName = 'Jennings';

-- Update Roland Keitel’s first name to “Dr. Roland”
update customers set contactFirstName = 'Dr. Roland' where contactLastName ='Keitel';

-- Update all spanish customers to be associated with employee Martin Gerard
select c.customerName, c.city, c.salesRepEmployeeNumber,
e.firstName, e.lastName 
from customers c, employees e where c.country = 'Spain'
and e.employeeNumber = c.salesRepEmployeeNumber; 

update customers c set c.salesRepEmployeeNumber = 1702 where c.country = 'Spain';

-- Update all motorcycles’ MSRP with extra 10% 
select MSRP from products where productLine = 'Motorcycles';
update products set MSRP = MSRP * 1.1 where productLine = 'Motorcycles';

-- Update all customers with null in address Line2 to an empty string
select * from customers where addressLine2 is null;
update customers set addressLine2 = '' where addressLine2 is null ;